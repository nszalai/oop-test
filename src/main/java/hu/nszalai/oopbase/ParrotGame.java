package hu.nszalai.oopbase;

import hu.nszalai.oopbase.commons.Color;
import hu.nszalai.oopbase.entity.Human;
import hu.nszalai.oopbase.entity.Parrot;

/**
 * Hello world!
 *
 */
public class ParrotGame 
{
    public static void main( String[] args )
    {
        
        Parrot toki = new Parrot(2, 2, Color.BLUE, "spagetti", "Toki");        
        Human mate = new Human("Mate", toki);
        
        mate.hungryParrot(true);
        mate.hungryParrot(false);
        mate.sleepParrot(true);
        mate.sleepParrot(false);
        mate.playParrot("hello");
        
        Human niki = new Human("Niki");
        niki.hungryParrot(true);
        niki.hungryParrot(false);
        niki.sleepParrot(true);
        niki.sleepParrot(false);
        niki.playParrot("hello");
    }
}
