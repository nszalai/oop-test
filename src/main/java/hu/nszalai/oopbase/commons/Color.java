package hu.nszalai.oopbase.commons;

/**
 *
 * @author nszalai
 */
public enum Color {
    BLUE("B"), YELLOW("Y"), GREEN("G"), RED("R"), WHITE("W");
            
    private static final String AND = " and ";
    
    private String color;
    
    private Color(String s) {
        color = s;
    }
    
    private Color(final String color1, final String color2) {
        StringBuilder mixedColor = new StringBuilder();
        mixedColor.append(color1);
        mixedColor.append(AND);
        mixedColor.append(color2);
                
        color = mixedColor.toString();
    }
    
    public String getColor() {
        return color;
    }

    
}
