package hu.nszalai.oopbase.commons;

/**
 *
 * @author nszalai
 */
public enum Words {

    HELLO("parrot_hello"), LOVE("parrot_love"), SMART("parrot_smart"), STUPID("parrot_stupid");

    private String word;

    private Words(String str) {
        word = str;
    }

    public static boolean contains(String word) {

        for (Words w : Words.values()) {
            System.out.println("DEBUG w: " + w);
            System.out.println("DEBUG word: " + word);
            if (w.name().equalsIgnoreCase(word)) {
                return true;
            }
        }

        return false;
    }

    public static int wordNumber () {
        return Words.values().length;
    }
    
    public String getWord() {
        return word;
    }
}
