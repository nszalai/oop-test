package hu.nszalai.oopbase.entity;

import hu.nszalai.oopbase.commons.Color;
import hu.nszalai.oopbase.commons.Words;

/**
 *
 * @author nszalai
 */
public class Parrot extends Bird {
    
    private int wordNumber;    
    private final static String NOT_SAY = " say nothing.";
    private final static String SAY = " say: ";
    
    public Parrot(final int wingNumber, final int legNumber, final Color featherColor, final String bestFeed, final String name) {
        setWingNumber(wingNumber);
        setLegNumber(legNumber);
        setFeatherColor(featherColor);
        setName(name);
        setBestFeed(bestFeed);
        this.wordNumber = Words.wordNumber();
    }

    public void say(final String word) {
        if (!Words.contains(word)) {            
            System.out.println(getName() + NOT_SAY);
        } else {
            
            for (Words w : Words.values()) {
                if (w.toString().equalsIgnoreCase(word)) {
                    System.out.println(getName() + SAY + w.getWord());
                }
            }
                        
        }
    }
        
    public int getWordNumber() {
        return wordNumber;
    }

    public void setWordNumber(int wordNumber) {
        this.wordNumber = wordNumber;
    }
            
}
