package hu.nszalai.oopbase.entity;

import hu.nszalai.oopbase.commons.Color;

/**
 *
 * @author nszalai
 */
public class Bird {

    private int wingNumber;
    private int legNumber;
    private Color featherColor;
    private String bestFeed;
    private String name;
    
    public void hungry(final boolean isHungry) {
        if (isHungry) {
           System.out.println(name + " is hungry.");
        } else {
            System.out.println(name + " is not hungry.");
        }

    }
    
    public void sleep(final boolean isSleep) {
        if (isSleep) {
           System.out.println(name + " is sleeping.");
        } else {
            System.out.println(name + " is not sleeping.");
        }
    }
        
    public String getBestFeed() {
        return bestFeed;
    }

    public void setBestFeed(String bestFeed) {
        this.bestFeed = bestFeed;
    }

    public Color getFeatherColor() {
        return featherColor;
    }

    public void setFeatherColor(Color featherColor) {
        this.featherColor = featherColor;
    }

    public int getLegNumber() {
        return legNumber;
    }

    public void setLegNumber(int legNumber) {
        this.legNumber = legNumber;
    }

    public int getWingNumber() {
        return wingNumber;
    }

    public void setWingNumber(int wingNumber) {
        this.wingNumber = wingNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    
}

