package hu.nszalai.oopbase.entity;

/**
 *
 * @author nszalai
 */
public class Human {

    private final static String SAY = "'s parrot: ";
    
    private String name;
    private boolean hasParrot;
    private Parrot parrot;

    public Human(String name) {
        this.name = name;
        this.hasParrot = false;
        this.parrot = null;
    }
    
    public Human(String name, Parrot parrot) {
        this.name = name;
        this.hasParrot = true;
        this.parrot = parrot;
    }
    
    public void playParrot(String play) {
        if (hasParrot) {
            System.out.println(name + SAY);
            parrot.say(play);
        } else {
            System.out.println("You can not play parrot becase you have not a parrot.");
        }
    }
    
    public void sleepParrot(boolean sleep) {
        if (hasParrot) {
            System.out.println(name + SAY);
            parrot.sleep(sleep);
        } else {
            System.out.println("You have not a parrot.");
        }
    }
    
    public void hungryParrot(boolean hungry) {
        if (hasParrot) {
            System.out.println(name + SAY);
            parrot.hungry(hungry);
        } else {
            System.out.println("You have not a parrot.");
        }
    }
}
